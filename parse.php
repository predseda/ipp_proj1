<?php

// Projekt do předmětu IPP 2019
// Autor: Jan Svoboda xsvobo0u@stud.fit.vutbr.cz

require_once('parse_utils.php');
require_once('core.php');

if($argc >= 2)
{
  if($argv[1] == "--help" && $argc == 2)
  {
    usage($argv);
  }
  else
  {
    exit(ERR_ARG_HELP);
  }
}

$stdin = open_stdin();
$xml = create_xml();

// checks for .IPPcode19 header
check_header($stdin);

parse($xml, $stdin);

save_and_exit($xml, $stdin);

?>
