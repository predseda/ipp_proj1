<?php

// Projekt do předmětu IPP 2019
// Autor: Jan Svoboda xsvobo0u@stud.fit.vutbr.cz

require_once('parse_utils.php');

const ERR_HEADER     = 21;
const ERR_OPCODE     = 22;
const ERR_LEX_OR_SYN = 23;

// Reads line from STDIN, trims new line character, converts the string
// to lowercase and returns the string.
function get_line($stdin)
{
  $line = fgets($stdin);
  $line = trim($line, "\n");

  return $line;
}

// Removes comment from string
function remove_comment($str)
{
  $str = preg_replace('/\s*#+.*/', '', $str);

  return $str;
}

// Checks for .IPPcode19 header.
function check_header($stdin)
{
  $header = get_line($stdin);
  $header = remove_comment($header);

  if(strtolower($header) !== '.ippcode19')
  {
    fprintf(STDERR, "invalid header!\n");
    exit(ERR_HEADER);
  }
}

// Parses operand passed by $operand
function parse_operand($operand, $line_number)
{
  if($operand === NULL)
  {
    return NULL;
  }

  $regex_int    = '/^int@.*/';
  $regex_string = '/^string@.*/';
  $regex_var_gf = '/^GF@.*/';
  $regex_var_lf = '/^LF@.*/';
  $regex_var_tf = '/^TF@.*/';
  $regex_bool   = '/^bool@.*/';

  $op = new Operand;

  if(preg_match($regex_int, $operand) === 1)
  {
    $op->type = OP_INT;

    $tokens = strtok($operand, '@');
    $op->value = strtok($tokens);
  }
  else if($operand === 'bool@true')
  {
    $op->type = OP_BOOL;
    $op->value = 'true';
  }
  else if($operand === 'bool@false')
  {
    $op->type = OP_BOOL;
    $op->value = 'false';
  }
  else if($operand === 'nil@nil')
  {
    $op->type = OP_NIL;
    $op->value = 'nil';
  }
  else if(preg_match($regex_string, $operand) === 1)
  {
    $op->type = OP_STRING;

    $op->value = strstr($operand, '@');
    $op->value = trim($op->value, '@');
    $op->value = htmlspecialchars($op->value);
  }
  else if($operand === 'int')
  {
    $op->type = OP_TYPE;
    $op->value = 'int';
  }
  else if($operand === 'bool')
  {
    $op->type = OP_TYPE;
    $op->value = 'bool';
  }
  else if($operand === 'string')
  {
    $op->type = OP_TYPE;
    $op->value = 'string';
  }
  else if(preg_match($regex_var_gf, $operand) === 1)
  {
    $op->type = OP_VAR_GF;

    $op->value = strstr($operand, '@');
    $op->value = trim($op->value, '@');
    $op->value = htmlspecialchars($op->value);
  }
  else if(preg_match($regex_var_lf, $operand) === 1)
  {
    $op->type = OP_VAR_LF;

    $op->value = strstr($operand, '@');
    $op->value = trim($op->value, '@');
    $op->value = htmlspecialchars($op->value);
  }
  else if(preg_match($regex_var_tf, $operand) === 1)
  {
    $op->type = OP_VAR_TF;

    $op->value = strstr($operand, '@');
    $op->value = trim($op->value, '@');
    $op->value = htmlspecialchars($op->value);
  }
  else
  {
    if(preg_match($regex_bool, $operand) === 1)
    {
      fprintf(STDERR, "invalid operand!\n");
      exit(ERR_LEX_OR_SYN);
    }

    $op->type = OP_LABEL;
    $op->value = $operand;
    $op->value = htmlspecialchars($op->value);
  }

  return $op;
}

// instruction with the same format: INSTR <var> <symb1> <symb2>
// ADD, SUB, MUL, IDIV, LT, GT, EQ, AND, OR, NOT, STRI2INT, CONCAT, GETCHAR, SETCHAR
function parse_same_instruction_format($instruction, $type, $operands, $line_number)
{
  if(count($operands) !== 3)
  {
    fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
    exit(ERR_LEX_OR_SYN);
  }

  $instruction->type = $type;
  $instruction->op1 = parse_operand($operands[0], $line_number);
  $instruction->op2 = parse_operand($operands[1], $line_number);
  $instruction->op3 = parse_operand($operands[2], $line_number);

  if($instruction->op1->type !== OP_VAR_GF &&
     $instruction->op1->type !== OP_VAR_LF &&
     $instruction->op1->type !== OP_VAR_TF)
  {
    fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
    exit(ERR_LEX_OR_SYN);
  }

  if($instruction->op2->type === OP_LABEL || $instruction->op2->type === OP_TYPE)
  {
    fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
    exit(ERR_LEX_OR_SYN);
  }

  if($instruction->op3->type === OP_LABEL || $instruction->op3->type === OP_TYPE)
  {
    fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
    exit(ERR_LEX_OR_SYN);
  }
}

// $tokens is array of instruction tokens
// $tokens[0] - opcode
// $tokens[1] - 1. operand or NULL
// $tokens[2] - 2. operand or NULL
// $tokens[3] - 3. operand or NULL
function parse_instruction($xml, $instruction, $tokens, $instruction_counter, $line_number)
{
  clean_instruction($instruction);

  $operands = array();
  //$operands[0] = $operands[1] = $operands[2] = NULL;

  if(!empty($tokens[1]))
    $operands[0] = $tokens[1];

  if(!empty($tokens[2]))
    $operands[1] = $tokens[2];

  if(!empty($tokens[3]))
    $operands[2] = $tokens[3];

  switch(strtolower($tokens[0]))
  {
    case 'move':
      if(count($operands) !== 2)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operands!\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = I_MOVE;
      $instruction->op1 = parse_operand($operands[0], $line_number);
      $instruction->op2 = parse_operand($operands[1], $line_number);

      if($instruction->op1->type !== OP_VAR_GF &&
         $instruction->op1->type !== OP_VAR_LF &&
         $instruction->op1->type !== OP_VAR_TF)
      {
        fprintf(STDERR, "syntax error on line %d: invalid 1. operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      if($instruction->op2->type === OP_LABEL &&
         $instruction->op2->type === OP_TYPE)
      {
        fprintf(STDERR, "syntax error on line %d: invalid 2. operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      break;

    case 'createframe':
      if(count($operands) !== 0)
      {
        fprintf(STDERR, "syntax error on line %d: instruction CREATEFRAME has no operands\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = I_CREATEFRAME;
      break;

    case 'pushframe':
      if(count($operands) !== 0)
      {
        fprintf(STDERR, "syntax error on line %d: instruction PUSHFRAME has no operands\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = I_PUSHFRAME;
      break;

    case 'popframe':
      if(count($operands) !== 0)
      {
        fprintf(STDERR, "syntax error on line %d: instruction POPFRAME has no operands\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = I_POPFRAME;
      break;

    case 'defvar':
      if(count($operands) !== 1)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operands\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = I_DEFVAR;
      $instruction->op1 = parse_operand($operands[0], $line_number);

      if($instruction->op1->type !== OP_VAR_GF &&
         $instruction->op1->type !== OP_VAR_LF &&
         $instruction->op1->type !== OP_VAR_TF)
      {
        fprintf(STDERR, "syntax error on line %d: operand must be variable\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      break;

    case 'call':
      if(count($operands) !== 1)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = I_CALL;
      $instruction->op1 = new Operand;
      $instruction->op1->type = OP_LABEL;
      $instruction->op1->value = $operands[0];

      break;

    case 'return':
      if(count($operands) !== 0)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = I_RETURN;

      break;

    case 'pushs':
      if(count($operands) !== 1)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = I_PUSHS;
      $instruction->op1 = parse_operand($operands[0], $line_number);

      // operand must be <symb>
      if($instruction->op1->type === OP_LABEL ||
         $instruction->op1->type === OP_TYPE)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      break;

    case 'pops':
      if(count($operands) !== 1)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = I_POPS;
      $instruction->op1 = parse_operand($operands[0], $line_number);

      if($instruction->op1->type !== OP_VAR_GF &&
         $instruction->op1->type !== OP_VAR_LF &&
         $instruction->op1->type !== OP_VAR_TF)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      break;

    case 'add':
      parse_same_instruction_format($instruction, I_ADD, $operands, $line_number);
      break;

    case 'sub':
      parse_same_instruction_format($instruction, I_SUB, $operands, $line_number);
      break;

    case 'mul':
      parse_same_instruction_format($instruction, I_MUL, $operands, $line_number);
      break;

    case 'idiv':
      parse_same_instruction_format($instruction, I_IDIV, $operands, $line_number);
      break;

    case 'lt':
      parse_same_instruction_format($instruction, I_LT, $operands, $line_number);
      break;

    case 'gt':
      parse_same_instruction_format($instruction, I_GT, $operands, $line_number);
      break;

    case 'eq':
      parse_same_instruction_format($instruction, I_EQ, $operands, $line_number);
      break;

    case 'and':
      parse_same_instruction_format($instruction, I_AND, $operands, $line_number);
      break;

    case 'or':
      parse_same_instruction_format($instruction, I_OR, $operands, $line_number);
      break;

    case 'not':
      if(count($operands) !== 2)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = I_NOT;
      $instruction->op1 = parse_operand($operands[0], $line_number);
      $instruction->op2 = parse_operand($operands[1], $line_number);

      if($instruction->op1->type !== OP_VAR_GF &&
         $instruction->op1->type !== OP_VAR_LF &&
         $instruction->op1->type !== OP_VAR_TF)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      if($instruction->op2->type === OP_LABEL || $instruction->op2->type === OP_TYPE)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      break;

    case 'int2char':
      if(count($operands) !== 2)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = I_INT2CHAR;
      $instruction->op1 = parse_operand($operands[0], $line_number);
      $instruction->op2 = parse_operand($operands[1], $line_number);

      if($instruction->op1->type !== OP_VAR_GF &&
         $instruction->op1->type !== OP_VAR_LF &&
         $instruction->op1->type !== OP_VAR_TF)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      if($instruction->op2->type === OP_LABEL || $instruction->op2->type === OP_TYPE)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      break;

    case 'stri2int':
      parse_same_instruction_format($instruction, I_STRI2INT, $operands, $line_number);
      break;

    case 'read':
      if(count($operands) !== 2)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = I_READ;
      $instruction->op1 = parse_operand($operands[0], $line_number);
      $instruction->op2 = parse_operand($operands[1], $line_number);

      if($instruction->op1->type !== OP_VAR_GF &&
         $instruction->op1->type !== OP_VAR_LF &&
         $instruction->op1->type !== OP_VAR_TF)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      if($instruction->op2->type !== OP_TYPE)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      break;

    case 'write':
      if(count($operands) !== 1)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = I_WRITE;
      $instruction->op1 = parse_operand($operands[0], $line_number);

      if($instruction->op1->type === OP_LABEL || $instruction->op1->type === OP_TYPE)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      break;

    case 'concat':
      parse_same_instruction_format($instruction, I_CONCAT, $operands, $line_number);
      break;

    case 'strlen':
      if(count($operands) !== 2)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = 'I_STRLEN';
      $instruction->op1 = parse_operand($operands[0], $line_number);
      $instruction->op2 = parse_operand($operands[1], $line_number);

      if($instruction->op1->type !== OP_VAR_GF &&
         $instruction->op1->type !== OP_VAR_LF &&
         $instruction->op1->type !== OP_VAR_TF)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      if($instruction->op2->type === OP_LABEL || $instruction->op2->type === OP_TYPE)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      break;

    case 'getchar':
      parse_same_instruction_format($instruction, I_GETCHAR, $operands, $line_number);
      break;

    case 'setchar':
      parse_same_instruction_format($instruction, I_SETCHAR, $operands, $line_number);
      break;

    case 'type':
      if(count($operands) !== 2)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = I_TYPE;
      $instruction->op1 = parse_operand($operands[0], $line_number);
      $instruction->op2 = parse_operand($operands[1], $line_number);

      if($instruction->op1->type !== OP_VAR_GF &&
         $instruction->op1->type !== OP_VAR_LF &&
         $instruction->op1->type !== OP_VAR_TF)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      if($instruction->op2->type === OP_LABEL || $instruction->op2->type === OP_TYPE)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }
      break;

    case 'label':
      if(count($operands) !== 1)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = I_LABEL;
      $instruction->op1 = parse_operand($operands[0], $line_number);

      if($instruction->op1->type !== OP_LABEL)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      break;

    case 'jump':
      if(count($operands) !== 1)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = I_JUMP;
      $instruction->op1 = parse_operand($operands[0], $line_number);

      if($instruction->op1->type !== OP_LABEL)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      break;

    case 'jumpifeq':
      if(count($operands) !== 3)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = I_JUMPIFEQ;
      $instruction->op1 = parse_operand($operands[0], $line_number);
      $instruction->op2 = parse_operand($operands[1], $line_number);
      $instruction->op3 = parse_operand($operands[2], $line_number);

      if($instruction->op1->type !== OP_LABEL)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      if($instruction->op2->type === OP_LABEL || $instruction->op2->type === OP_TYPE)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      if($instruction->op3->type === OP_LABEL || $instruction->op3->type === OP_TYPE)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      break;

    case 'jumpifneq':
      if(count($operands) !== 3)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = I_JUMPIFNEQ;
      $instruction->op1 = parse_operand($operands[0], $line_number);
      $instruction->op2 = parse_operand($operands[1], $line_number);
      $instruction->op3 = parse_operand($operands[2], $line_number);

      if($instruction->op1->type !== OP_LABEL)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      if($instruction->op2->type === OP_LABEL || $instruction->op2->type === OP_TYPE)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      if($instruction->op3->type === OP_LABEL || $instruction->op3->type === OP_TYPE)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      break;

    case 'exit':
      if(count($operands) !== 1)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = I_EXIT;
      $instruction->op1 = parse_operand($operands[0], $line_number);

      if($instruction->op1->type === OP_LABEL || $instruction->op1->type === OP_TYPE)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      break;

    case 'dprint':
      if(count($operands) !== 1)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = I_DPRINT;
      $instruction->op1 = parse_operand($operands[0], $line_number);

      if($instruction->op1->type === OP_LABEL || $instruction->op1->type === OP_TYPE)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      break;

    case 'break':
      if(count($operands) !== 0)
      {
        fprintf(STDERR, "syntax error on line %d: invalid operand\n", $line_number);
        exit(ERR_LEX_OR_SYN);
      }

      $instruction->type = I_BREAK;

      break;

    default:
      fprintf(STDERR, "invalid instruction opcode\n");
      exit(ERR_OPCODE);
      break;
  }

  xml_instruction($xml, $instruction_counter, $instruction);
}

// The main parsing loop
function parse($xml, $stdin)
{
  $instruction = new Instruction;
  $instruction_counter = 1;
  $line_number = 1;

  while($line = fgets($stdin))
  {
    $line = trim($line, "\n");
    $line = remove_comment($line);
    $line_number++;

    if(empty($line))
      continue;

    // $tokens[0] contains opcode of instruction
    // $tokens[1], $tokens[2], $tokens[3] contain operands
    $tokens = preg_split('/\s/', $line);
    parse_instruction($xml, $instruction, $tokens, $instruction_counter, $line_number);
    $instruction_counter++;
  }
}

?>
