# The base of IPPcode19
IPPcode19 is unstructured imperative language consisting of three-address instructions.  
Each instruction consists of case-insensitive opcode (keyword with instruction name)  
and case-sensitive operands. Operands are delimited by any non-zero amount of spaces  
or tabulators. Each instruction is delimited by new line character.  
Each operand consists of variable, constant, type or label. IPPcode19 supports  
one-line comments starting with `#`.

**IPPcode19 code starts with line** `.IPPcode19` (case-insensitive).

# Memory model
During interpretation, values are usually stored to named variables,  
which are grouped into _frames_. Frames are dictionaries of variables and their values.  
IPPcode19 has three types of frames.

## Global frame
Global frame (**GF**) is at the very beginning of interpretation initialized to empty.  
It's purpose is storing global variables.

## Local frame
Local frame (**LF**) is at the very beginning of interpretation undefined.  
It points to top of _stack frame_. It's purpose is storing local variables  
of functions. Stack frame can be used for recursive function calls.  
Local frames, which are not at the top of stack frame cannot be accessed.

## Temporary frame
Temporary frame (**TF**) is at the very beginning of interpretation undefined.  
It's purpose is preparing for new or cleaning of old frame (e.g. before function call  
or return from function).  It can be moved to stack frame and become actual local frame.

# Data types
IPPcode19 is dynamic-typed. Implicit conversions are forbidden.  
Interpret supports special value/type _nil_ and three base data types - _int_, _bool_, _string_.  
Their values correspond with Python 3 data types.

Each IPPcode19 constant consists of two parts delimited by `@` - type of constant
and its value.  
Example: `bool@true`, `nil@nil`, `int@-5`.

Int represents integer. Bool represents values _true_ and _false_.  
Literal for type string is, in case of constant, represented as sequence of printable  
characters in UTF-8 encoding (excluding white characters, # and \) and escape sequences.  
It is not surrounded by quotation marks. Escape sequence, which is necessary  
for characfters with code 000-032, 035, 092 is in format: _\xyz_, where _xyz_  
is number between 000-999 consisting of three digits.  
String constant example: `string@Hello\032world!` is equivalent to `Hello world!`.

Access to non-existing or uninitialized variable leads to error.  
Interpretation of instruction with invalid types of operands leads to error as well.

# Instruction set
Description of operands are in non-terminal symbols in <>.  
Non-terminal _<var>_ is a variable, _<symb>_ is a constant or variable,  
_<label>_ is a label. Variable identifier consists of two parts delimited by `@`,  
frame type `LF`, `TF` or `GF` and name of the variable.  
Name of variable consists of sequence of alphanumeric characters, special characters  
without white characters, starting with a letter or special character.  
Possible special characters: `_`, `-`, `$`, `&`, `%`, `*`, `!`, `?`.  
E.g. `GF@_x` stands for variable with name `_x` in global frame.

Labels must be in same format as variable names.  
Example of IPPcode19 code:  
```
.IPPcode19
DEFVAR GF@counter
MOVE GF@counter string@  # initialization of variable to empty string

LABEL while
JUMPIFEQ end GF@counter string@aaa
WRITE string@counter\032contains\032
WRITE GF@counter
WRITE string@\010
CONCAT GF@counter GF@counter string@a
JUMP while
LABEL end
```

Instruction set contains instruction for dealing with variables in frames,  
jumps, stack manipulation, arithmetic, logic, relational operations, input/output  
and debug instructions.

## Frames manipulation and function calling
**MOVE** _<var>_ _<symb>_  
Copies value _<symb>_ to _<var>_. E.g. `MOVE LF@par GF@var` copies value of variable _var_  
in global frame to variable _par_ in local frame.

**CREATEFRAME**  
Creates new temporary frame and discards content of original temporary frame.

**PUSHFRAME**  
Moves _TF_ to stack frame. Frame becomes accessible via _LF_. _TF_ becomes undefined  
and must be recreated by instruction CREATEFRAME before its next usage.  
Attempt to access undefined frame leads to error.

**POPFRAME**  
Moves frame _LF_ from top of the stack frame to _TF_. If there is no frame at stack frame,  
usage of this instruction leads to error.

**DEFVAR** _<var>_  
Defines variable in frame by _<var>_. This variable is uninitialized and without type.  
The type will be defined by value assignment.

**CALL** _<label>_  
Stores incremented actual position of intern instruction counter to call stack  
and jumps to label _<label>_. Frames, if needed, must be prepared by other instructions.

**RETURN**  
Withdraws position from the call stack and jumps to this position by setting  
intern instruction counter. Frame cleaning must be done by other instructions.

## Data frame manipulation
**PUSHS** _<symb>_  
Stores value of _<symb>_ to data stack.

**POPS** _<var>_  
Withdraws value from data stack. If the data stack is empty, instruction leads to error.

## Arithmetic, relational, boolean and conversion instructions
**ADD** _<var>_ _<symb1>_ _<symb2>_  
Adds _<symb1>_ and _<symb2>_ (both must be of type int) and stores the result 
to variable _<var>_

**SUB** _<var>_ _<symb1>_ _<symb2>_  
Subtracts _<symb2>_ from _<symb1>_ (both must be of type int) and stores the result 
to variable _<var>_.

**MUL** _<var>_ _<symb1>_ _<symb2>_  
Multiplies _<symb1>_ by _<symb2>_ (both must be of type int) and stores the result 
to variable _<var>_.

**IDIV** _<var>_ _<symb1>_ _<symb2>_  
Divides (integer division) _<symb1>_ by _<symb2>_ (both must be of type int) and stores the result
to variable _<var>_. Division by zero leads to error.

**LT/GT/EQ** _<var>_ _<symb1>_ _<symb2>_  
Evaluates relational operator between _<symb1>_ and _<symb2>_ (must be the same type  
int, bool or string) and stores the result _true_ or _false_ to boolean variable _<var>_.  
String are compared lexicographically, _false_ is lesser than _true_.  
Operand of type _nil_ can be compared only by instruction EQ, otherwise instruction
leads to error.

**AND/OR/NOT** _<var>_ _<symb1>_ _<symb2>_  
Applies logical and/or/not to operands of type bool and stores the result to variable _<var>_.  
Not has only two operands.

**INT2CHAR** _<var>_ _<symb>_  
Integer value _<symb>_ is by Unicode specification converted to character,  
which makes one-character string stored to _<var>_. If _<symb>_ isn't a valid  
ordinal value of Unicoe character (see function _chr_ in Python 3), instruction
leads to error.

**STRI2INT** _<var>_ _<symb1>_ _<symb2>_  
Stores ordinal value of Unicode character in string _<symb1>_ on position _<symb2>_  
(indexed from 0) to variable _<var>_. Indexing beyond of the string leads to error.  
See function _ord_ in Python 3.

## Input-output instructions
**READ** _<var>_ _<type>_  
Reads one value of type _<type>_ (int/string/bool) and stores it to variable _<var>_.  
Reading is done by function _input()_ of Python 3. Conversion to type _<type>_  
is performed after the value is read. String 'true' is converted to `bool@true`,  
otherwise to `bool@false`. If the input is invalid variable _<var>_ contains  
its implicit value (0 for int, empty string for string, fale for bool).

**WRITE** _<symb>_  
Writes value of _<symb>_ to standard output. The output format is compatible with
function _print_ of Python 3  
with parameter `end=''`, except for type bool.

## Strings manipulation
**CONCAT** _<var>_ _<symb1>_ _<symb2>_  
Stores string created by concatenation of strings _<symb1>_ and _<symb2>_  
to variable _<var>_. Other types are not permitted.

**STRLEN** _<var>_ _<symb>_  
Stores length of string _<symb>_ to variable _<var>_.

**GETCHAR** _<var>_ _<symb1>_ _<symb2>_  
Stores 1-character string from _<symb1>_ on position _<symb2>_ to variable _<var>_.  
Indexing beyond the string leads to error.

**SETCHAR** _<var>_ _<symb1>_ _<symb2>_  
Modifies character in string _<var>_ on position _<symb1>_ to character _<symb2>_  
(first character of _<symb2>_, if it contains more than one character).  
Indexing beyond the string or if _<symb2>_ is mepty string leads to error.

## Data types manipulation
**TYPE** _<var>_ _<symb>_  
Stores string representing the data type of _<symb>_ to variable _<var>_.  
If _<symb>_ is uninitialized variable, its type is empty string.

## Flow control instructions
If _<label>_ does not exist, instructions leads to error.

**LABEL** _<label>_  
Creates label _<label>_. Attempt of label redefinition leads to error.

**JUMP** _<label>_  
Unconditionally jumps to label _<label>_.

**JUMPIFEQ** _<label>_ _<symb1>_ _<symb2>_  
If _<symb1>_ and _<symb2>_ have the same type (otherwise instruction leads to error)  
and their values are equal, jump to label _<label>_ is performed.

**JUMPIFNEQ** _<label>_ _<symb1>_ _<symb2>_  
If _<symb1>_ and _<symb2>_ have the same type (otherwise instruction leads to error)  
and their values are _not_ equal, jump to label _<label>_ is performed.

**EXIT** _<symb>_  
Quits the program with return value _<symb>_ (int with value 0 to 49).  
Invalid int (or non-int) value leads to error.

## Debug instructions
**DPRINT** _<symb>_  
Prints value of _<symb>_ to standard error output (stderr).

**BREAK**  
Prints state of the interpret to standard output.  
State of the interpred is defined as: instruction order, frames content,  
count of already performed instructions at the moment.
