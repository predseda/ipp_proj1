Implementační dokumentace k 1. úloze do IPP 2018/2019  
Jméno a příjmení: Jan Svoboda  
Login: xsvobo0u  

# Zpracování instrukcí jazyka IPPcode19
Nejprve se zkontroluje hlavička _.IPPcode19_. Pokud je v pořádku,
pokračuje se v syntaktické analýze, jinak skript skončí
s návratovým kódem 21.

## Syntaktická analýza
### Načítání instrukcí
Skript čte po řádcích ze standardního vstupu.
Z každého načteného řádku odstraní komentář a znak konce řádku.
Následně rozdělí řádek na 4 části: název instrukce a 3 operandy.
Pokud název instrukce odpovídá instrukci jazyka IPPcode19,
proběhne validace operandů, jinak skript skončí s návratovým kódem 22.

### Validace operandů
Nejprve je zkontrolován počet operandů. Pokud je u instrukce správný
počet operandů, následuje jejich kontrola, jinak skript skončí
s návratovým kódem 23.
Poslední kontrola operandů probíhá pomocí regulárních výrazů - platné datové typy
jazyka IPPcode19. Samotné hodnoty se nekontrolují. Tedy např. operand `bool@asdf`
je vyhodnocen jako platná konstanta typu bool a operand bude uložen do výsledného XML.
Pokud je celá instrukce v pořádku, vygenerují se příslušné XML elementy.

## Generování XML
Pro generování XML elementů jsem použil knihovnu XMLWriter.
XML elementy jednotlivých instrukcí jsou generovány během
syntaktické analýzy. Celé XML je vypsáno na standardní výstup
až na konci celé analýzy v případě, že ve vstupním programu
nebyla objevena syntaktická nebo lexikální chyba.

## Datový typ pro instrukci jazyka IPPcode19
Pro instrukce jsem vytvořil třídu Instruction (symbolický zápis):
```
class Instruction
{
  opcode
  operand1
  operand2
  operand3
}
```

Modifikátory přístupů všech atributů jsou public.
Třída nemá (kromě funkce pro inicializaci) žádné metody.
Instruction je tedy sémanticky blíže struktuře jazyka C
a funkcionálnímu programování, než OOP.
V celém skriptu existuje pouze jedna instance třídy Instruction.
Vzhledem k tomu, že s objektem této třídy pracuji jako se strukturou
jazyka C, tento koncept přibližně odpovídá návrhovému vzoru _singleton_.
