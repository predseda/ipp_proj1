<?php

// Projekt do předmětu IPP 2019
// Autor: Jan Svoboda xsvobo0u@stud.fit.vutbr.cz

require_once('instruction.php');

const ERR_ARG_HELP  = 10;
const ERR_IO_INPUT  = 11;
const ERR_IO_OUTPUT = 12;
const ERR_INTERNAL  = 99;

function usage($argv)
{
  printf("php7.3 ./parse.php\n");
}

// Opens STDIN and returns handle to STDIN or exits if fopen() fails.
function open_stdin()
{
  $stdin = fopen("php://stdin", "r");
  if($stdin === NULL)
  {
    fprintf(STDERR, "failed to open STDIN\n");
    exit(ERR_IO_INPUT);
  }

  return $stdin;
}

// Creates the XML, and generates the XML header.
// Returns the XML resource or exits if XML creation fails.
function create_xml()
{
  $xml = xmlwriter_open_memory();
  if($xml === FALSE)
  {
    fprintf(STDERR, "xmlwriter_open_memory() failed!\n");
    exit(ERR_INTERNAL);
  }

  xmlwriter_start_document($xml, '1.0', 'UTF-8');
  xmlwriter_set_indent($xml, 2);

  // root element <program language="IPPcode19">
  xmlwriter_start_element($xml, 'program');
  xmlwriter_start_attribute($xml, 'language');
  xmlwriter_text($xml, 'IPPcode19');
  xmlwriter_end_attribute($xml);

  return $xml;
}

// Saves the generated XML to the output file and closes opened files.
function save_and_exit($xml, $stdin)
{
  xmlwriter_end_element($xml);  // root element <program>

  xmlwriter_end_document($xml);

  echo xmlwriter_output_memory($xml);
  fclose($stdin);
}

// Appends IPPcode19 operands of instruction to the XML.
function xml_operand($xml, $op_number, $op)
{
  $arg = 'arg' . $op_number;

  xmlwriter_start_element($xml, $arg);
  xmlwriter_start_attribute($xml, 'type');
  switch($op->type)
  {
    case OP_INT:
      xmlwriter_text($xml, 'int');
      break;

    case OP_BOOL:
      xmlwriter_text($xml, 'bool');
      break;

    case OP_STRING:
      xmlwriter_text($xml, 'string');
      break;

    case OP_NIL:
      xmlwriter_text($xml, 'nil');
      break;

    case OP_VAR_GF:
    case OP_VAR_LF:
    case OP_VAR_TF:
      xmlwriter_text($xml, 'var');
      break;

    case OP_LABEL:
      xmlwriter_text($xml, 'label');
      break;

    case OP_TYPE:
      xmlwriter_text($xml, 'type');
      break;
  }
  xmlwriter_end_attribute($xml);  // arg

  // if operand is a variable, we must add prefix GF@/LF@/TF@ to the name of the variable
  if($op->type == OP_VAR_GF)
    $op->value = 'GF@' . $op->value;
  else if($op->type == OP_VAR_LF)
    $op->value = 'LF@' . $op->value;
  else if($op->type == OP_VAR_TF)
    $op->value = 'TF@' . $op->value;

  xmlwriter_write_raw($xml, $op->value);

  xmlwriter_end_element($xml);  // arg
}

// Appends IPPcode19 instruction to the XML.
function xml_instruction($xml, $order, $instruction)
{
  xmlwriter_start_element($xml, 'instruction');

  xmlwriter_start_attribute($xml, 'order');
  xmlwriter_write_raw($xml, $order);
  xmlwriter_end_attribute($xml);  // order

  xmlwriter_start_attribute($xml, 'opcode');
  switch($instruction->type)
  {
    case I_MOVE:
      xmlwriter_text($xml, 'MOVE');
      break;

    case I_CREATEFRAME:
      xmlwriter_text($xml, 'CREATEFRAME');
      break;

    case I_PUSHFRAME:
      xmlwriter_text($xml, 'PUSHFRAME');
      break;

    case I_POPFRAME:
      xmlwriter_text($xml, 'POPFRAME');
      break;

    case I_DEFVAR:
      xmlwriter_text($xml, 'DEFVAR');
      break;

    case I_CALL:
      xmlwriter_text($xml, 'CALL');
      break;

    case I_RETURN:
      xmlwriter_text($xml, 'RETURN');
      break;

    case I_PUSHS:
      xmlwriter_text($xml, 'PUSHS');
      break;

    case I_POPS:
      xmlwriter_text($xml, 'POPS');
      break;

    case I_ADD:
      xmlwriter_text($xml, 'ADD');
      break;

    case I_SUB:
      xmlwriter_text($xml, 'SUB');
      break;

    case I_MUL:
      xmlwriter_text($xml, 'MUL');
      break;

    case I_IDIV:
      xmlwriter_text($xml, 'IDIV');
      break;

    case I_LT:
      xmlwriter_text($xml, 'LT');
      break;

    case I_GT:
      xmlwriter_text($xml, 'GT');
      break;

    case I_EQ:
      xmlwriter_text($xml, 'EQ');
      break;

    case I_AND:
      xmlwriter_text($xml, 'AND');
      break;

    case I_OR:
      xmlwriter_text($xml, 'OR');
      break;

    case I_NOT:
      xmlwriter_text($xml, 'NOT');
      break;

    case I_INT2CHAR:
      xmlwriter_text($xml, 'INT2CHAR');
      break;

    case I_STRI2INT:
      xmlwriter_text($xml, 'STRI2INT');
      break;

    case I_READ:
      xmlwriter_text($xml, 'READ');
      break;

    case I_WRITE:
      xmlwriter_text($xml, 'WRITE');
      break;

    case I_CONCAT:
      xmlwriter_text($xml, 'CONCAT');
      break;

    case I_STRLEN:
      xmlwriter_text($xml, 'STRLEN');
      break;

    case I_GETCHAR:
      xmlwriter_text($xml, 'GETCHAR');
      break;

    case I_SETCHAR:
      xmlwriter_text($xml, 'SETCHAR');
      break;

    case I_TYPE:
      xmlwriter_text($xml, 'TYPE');
      break;

    case I_LABEL:
      xmlwriter_text($xml, 'LABEL');
      break;

    case I_JUMP:
      xmlwriter_text($xml, 'JUMP');
      break;

    case I_JUMPIFEQ:
      xmlwriter_text($xml, 'JUMPIFEQ');
      break;

    case I_JUMPIFNEQ:
      xmlwriter_text($xml, 'JUMPIFNEQ');
      break;

    case I_EXIT:
      xmlwriter_text($xml, 'EXIT');
      break;

    case I_DPRINT:
      xmlwriter_text($xml, 'DPRINT');
      break;

    case I_BREAK:
      xmlwriter_text($xml, 'BREAK');
      break;
  }
  xmlwriter_end_attribute($xml);  // opcode

  // operands
  if($instruction->op1 !== NULL)
    xml_operand($xml, '1', $instruction->op1);

  if($instruction->op2 !== NULL)
    xml_operand($xml, '2', $instruction->op2);

  if($instruction->op3 !== NULL)
    xml_operand($xml, '3', $instruction->op3);

  xmlwriter_end_element($xml);  // instruction

  clean_instruction($instruction);
}

?>
