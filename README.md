This is the first part of school project for [IPP](https://www.fit.vutbr.cz/study/course-l.php.en?id=12776) course at [BUT FIT](https://www.fit.vutbr.cz/).  
The second part is [here](https://gitlab.com/vut_fit/ipp/ipp_proj2).

# Task
This part of project is lexical and syntactic analyzer of _IPPcode19_,  
which is 3-address code developed especially for this project.  
It reads the instructions from standard input and does the lexical and syntactic  
analysis. If the input is correct, the script creates XML with the IPPcode19  
instructions and prints it to the standard output.

**The IPPcode19 description is [here](./doc/ippcode19_description.md).**

## Output XML format
The XML header must be: `<?xml version="1.0" encoding="UTF-8"?>`.  
The header must be followed by element _program_ with attribute _language_  
with `IPPcode19` value. This element contains _instruction_ elements.  
Each _instruction_ element consists of attribute _order_ with value of instruction order  
starting from 1, attribute _opcode_ with value of instruction opcode and elements  
for operands _arg1_ for 1. operand, _arg2_ for 2. operand and _arg3_ for 3. operand.  
Operand elements have attribute _type_ with possible values _int_, _bool_, _string_,  
_nil_, _label_, _type_, _var_.

This text element has value of literal (without type and `@` character) or label  
or type or identifier of variable (including frame type and `@`).

At _string_ literals, don't convert IPPcode19 escape sequences. Covert just characters  
that are not suitable for XML (e.g. <, >, &) - use appropriate XML entities,  
e.g. `&lt;`, `&gt;`, `&amp;`, etc.  
Also convert variable names which are not XML-suitable the same way.

Analysis of IPPcode19 is context-sensitive, so e.g. keyword can be used as label.

# Requirements
PHP >= 7.3 (very likely will work with PHP 5.x)

# How to run the script
`php parse.php <instructions.ippcode19 > out.xml`  
Reads IPPcode19 instructions from _instructions.ippcode19_ file, does the analysis,  
creates XML with the instructions and prints it to the _out.xml_ file.
