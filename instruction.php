<?php

// Projekt do předmětu IPP 2019
// Autor: Jan Svoboda xsvobo0u@stud.fit.vutbr.cz

// operand types
const OP_INT    = 50;
const OP_BOOL   = 51;
const OP_STRING = 52;
const OP_NIL    = 53;
const OP_VAR_GF = 54;
const OP_VAR_LF = 55;
const OP_VAR_TF = 56;
const OP_LABEL  = 57;
const OP_TYPE   = 58;

class Operand
{
  public $type;
  public $value;
}

// instruction types
const I_MOVE        = 0;
const I_CREATEFRAME = 1;
const I_PUSHFRAME   = 2;
const I_POPFRAME    = 3;
const I_DEFVAR      = 4;
const I_CALL        = 5;
const I_RETURN      = 6;
const I_PUSHS       = 7;
const I_POPS        = 8;
const I_ADD         = 9;
const I_SUB         = 10;
const I_MUL         = 11;
const I_IDIV        = 12;
const I_LT          = 13;
const I_GT          = 14;
const I_EQ          = 15;
const I_AND         = 16;
const I_OR          = 17;
const I_NOT         = 18;
const I_INT2CHAR    = 19;
const I_STRI2INT    = 20;
const I_READ        = 21;
const I_WRITE       = 22;
const I_CONCAT      = 23;
const I_STRLEN      = 24;
const I_GETCHAR     = 25;
const I_SETCHAR     = 26;
const I_TYPE        = 27;
const I_LABEL       = 28;
const I_JUMP        = 29;
const I_JUMPIFEQ    = 30;
const I_JUMPIFNEQ   = 31;
const I_EXIT        = 32;
const I_DPRINT      = 33;
const I_BREAK       = 34;

// IPPcode19 instruction
class Instruction
{
  public $type;
  public $op1;
  public $op2;
  public $op3;
}

// Sets the instruction properties to NULL.
// It is called after the instruction is appended to the XML.
function clean_instruction($instruction)
{
  $instruction->type = NULL;
  $instruction->op1 = NULL;
  $instruction->op2 = NULL;
  $instruction->op3 = NULL;
}

?>
